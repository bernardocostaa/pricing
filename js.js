const btnToggle = document.querySelector('.toggle')

btnToggle.addEventListener('click', () =>{
    let price1 = document.getElementById('price-1')
    console.log(price1);
    let price2 = document.getElementById('price-2')
    let price3 = document.getElementById('price-3')
    btnToggle.classList.toggle('ativo')

    if(btnToggle.classList.contains('ativo')){
        price1.innerText = '199.99'
        price2.innerText = '249.99'
        price3.innerText = '399.99'
    }else{
        price1.innerText = '19.99'
        price2.innerText = '24.99'
        price3.innerText = '39.99'
    }

})